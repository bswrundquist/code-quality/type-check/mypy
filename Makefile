FQN := bswrundquist-code-quality-type-check-mypy

build:
	docker build -t ${FQN} .

help: build
	docker run -it --rm ${FQN} --help

refresh:
	docker stop ${FQN} || true
	docker rm ${FQN} || true
	docker rmi ${FQN} || true

smoke-test: build
	docker run -it --rm ${FQN} mypy test.py

introspect: build
	docker run -it --rm ${FQN} bash

clean:
	rm -rf .coverage
	rm -rf .idea
	rm -rf  __pycache__