from pandas import util

import pandas as pd

import uuid

import sys

sys.path.append(".")
import main  # noqa: E402


df = util.testing.makeMixedDataFrame()


def test_types():
    df = pd.DataFrame(
        {
            "char_col": ["hi", "there", "everyone"],
            "date_col": ["2020-01-01", "2020-01-02", "2020-01-03"],
            "numeric_col": [1.2, 2.3, 12.9],
        }
    )

    assert "char_col" in df.columns.tolist(), "char_col found."

def test_placeholder_method():
    from main import placeholder_method

    x = placeholder_method("hi")

    assert x == "hi", "Did not pass assertion"
